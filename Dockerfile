# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
#Working directory
WORKDIR D:\E\MicroservicesFullStack\Jenkins_Home\workspace\Meru\ProductConfigServer\target\
# copy fat WAR
COPY ProductConfigServer-0.0.1-SNAPSHOT.jar /ConfigServer.jar 
# runs application
CMD ["/usr/bin/java", "-jar", "-Dspring.profiles.active=default", "/ConfigServer.jar"]